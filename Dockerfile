# syntax=docker/dockerfile:1
FROM jupyter/tensorflow-notebook:2023-01-16

# https://agox.gitlab.io/agox/installation.html
# https://github.com/ppdebreuck/modnet/blob/master/setup.py

# ================================================================================
# Linux applications and libraries
# ================================================================================

USER root

RUN apt-get update --yes \
 && apt-get install --yes --no-install-recommends \
    openmpi-bin \
    libopenmpi-dev \
    cmake \
    libblas-dev \
    liblapack-dev \
    gfortran \
 && apt-get clean && rm -rf /var/lib/apt/lists/*

USER ${NB_UID}

RUN cd /tmp \
 && wget "https://github.com/lammps/lammps/archive/stable.zip" \
 && unzip -q stable.zip; rm stable.zip \
 && cd lammps-stable \
 && mkdir build; cd build \
 && cmake ../cmake -D PKG_ML-QUIP=yes -D DOWNLOAD_QUIP=yes -D USE_INTERNAL_LINALG=yes \
 && make -j4 \
 && make install\
 && rm -rf /tmp/lammps-stable



# ================================================================================
# Python environment
# ================================================================================

RUN mamba install --quiet --yes \
    "nbgitpuller" \
 && mamba clean --all -f -y \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"


RUN pip install --prefer-binary --no-cache-dir \
    "git+https://gitlab.com/agox/agox.git@stable#egg=agox" \
    "git+https://github.com/ppdebreuck/modnet.git@master#egg=modnet" \
    "quippy-ase" \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"

# Switch back to jovyan to avoid accidental container runs as root
USER ${NB_UID}
WORKDIR "${HOME}"

RUN <<EOT
#!/usr/bin/env python
from modnet.ext_data import load_ext_dataset
load_ext_dataset('MP_210321', 'feature_db')
EOT

COPY --chown=${NB_UID} tutorials/ .

ENV PATH="${PATH}:~/.local/bin/"
